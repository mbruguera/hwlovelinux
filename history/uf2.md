# COMMANDS

## Placa Base

*# dmidecode -t baseboard (info de la placa base)*

-t options:

    1. bios
    2. system
    3. baseboard
    4. chasis
    5. processor
    6. memory (se refiere a la RAM)
    7. cache
    8. connector
    9. slot
## RAM

*dmidecode -t memory*

## Disco Duro
### Herramienta smartctl

smartctl

¿Como saber el modelo del disco duro?

*smartctl -i /dev/sdx*

¿Como saber si mi disco duro ha tenido errores?

*smartctl-l error /dev/sdx*

¿Como saber cuantos bits ha escrito mi disco duro?

*smartctl -a*

### Herramienta lshw

    lshw --help
    man lshw

Una de las opciones par saber más informacion sobre nuestros discos duros es la herramienta *lshw -c o lshw -class*

      -class class
              Only  show the given class of hardware. class can be found using
              lshw -short or lshw -businfo.

       -C class
              Alias for -class class.

Pero te pide el tipo de bus que buscas. Eso se puede encontrar en *lshw -businfo*.

```
[root@a10 ism49298715]# lshw -businfo
Bus info          Device     Class          Description
=======================================================
                             system         H81M-S2PV (To be filled by O.E.M.)
                             bus            H81M-S2PV
                             memory         64KiB BIOS
                             memory         128KiB L1 cache
                             memory         512KiB L2 cache
                             memory         3MiB L3 cache
                             memory         8GiB System Memory
                             memory         4GiB DIMM DDR3 Synchronous 1400 MHz 
                             memory         DIMM [empty]
                             memory         4GiB DIMM DDR3 Synchronous 1400 MHz 
                             memory         DIMM [empty]
cpu@0                        processor      Intel(R) Pentium(R) CPU G3250 @ 3.20
pci@0000:00:00.0             bridge         4th Gen Core Processor DRAM Controll
pci@0000:00:02.0             display        Xeon E3-1200 v3/4th Gen Core Process
pci@0000:00:03.0             multimedia     Xeon E3-1200 v3/4th Gen Core Process
pci@0000:00:14.0             bus            8 Series/C220 Series Chipset Family 
usb@3             usb3       bus            xHCI Host Controller
usb@3:1                      input          USB Keyboard
usb@3:2                      input          USB Optical Mouse
usb@4             usb4       bus            xHCI Host Controller
pci@0000:00:16.0             communication  8 Series/C220 Series Chipset Family 
pci@0000:00:1a.0             bus            8 Series/C220 Series Chipset Family 
usb@1             usb1       bus            EHCI Host Controller
usb@1:1                      bus            Integrated Rate Matching Hub
pci@0000:00:1b.0             multimedia     8 Series/C220 Series Chipset High De
pci@0000:00:1c.0             bridge         8 Series/C220 Series Chipset Family 
pci@0000:00:1c.2             bridge         8 Series/C220 Series Chipset Family 
pci@0000:02:00.0  enp2s0     network        RTL8111/8168/8411 PCI Express Gigabi
pci@0000:00:1c.3             bridge         82801 PCI Bridge
pci@0000:03:00.0             bridge         82801 PCI Bridge
pci@0000:00:1d.0             bus            8 Series/C220 Series Chipset Family 
usb@2             usb2       bus            EHCI Host Controller
usb@2:1                      bus            Integrated Rate Matching Hub
pci@0000:00:1f.0             bridge         H81 Express LPC Controller
pci@0000:00:1f.2  scsi4      storage        8 Series/C220 Series Chipset Family 
scsi@4:0.0.0      /dev/sda   disk           500GB ST500DM002-1BD14
scsi@4:0.0.0,1    /dev/sda1  volume         205GiB Extended partition
                  /dev/sda5  volume         100GiB EXT4 volume
                  /dev/sda6  volume         100GiB EXT4 volume
                  /dev/sda7  volume         5117MiB Linux swap volume
pci@0000:00:1f.3             bus            8 Series/C220 Series Chipset Family 
                             system         PnP device PNP0c01
                             system         PnP device PNP0c02
                             system         PnP device PNP0b00
                             generic        PnP device INT3f0d
                             system         PnP device PNP0c02
                             communication  PnP device PNP0501
                             printer        PnP device PNP0400
                             system         PnP device PNP0c02
                             system         PnP device PNP0c02
                             power          To Be Filled By O.E.M.
```

### Herramienta lsblk
Lo que hace esta herramienta es imprimir por pantalla los dispositivos de bloques.

```
[root@a10 ism49298715]# lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]
```




