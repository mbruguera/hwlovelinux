boot = arrencada / procés d'arrencada
    interruptor
        verificació = POST de la BIOS
            dispositius d'arrencada (boot device)
                HD (hard disk)
                USB
                CD
                Xarxa - Protocol PXE

Tecla PAUSE/INTER = Pause la info de BIOS

### Kernel (Nucli)
Android
TV
S.O.
    Distro (distribució de Linux)
        Debian = Ubuntu, mint ...
        Red Hat = Fedora, CentOS...
# INSTALACIÓN
**Quemar USB (ISO to USB LIVE)**

```
sudo dd if=(donde esta la ISO) of=/dev/sdx bs=8M
```

**Partición dual WIN + FED**
WIN 
    MiPC/Mi Equipo
        C:/
            Propiedades
                Desfragmentar

Administración de Equipos
    Administración de disco
        Reducir Volumen (x)

**BIOS**
Boot menu
    escojer metodo (USB, PXe)

**Partición dial (instalando desde Fedora)**
Decir donde lo quieres instalar.
sdx sdy sdz ...
poner passwd de rood + user.

# Hardware
## Procesador
Lo importat d'un processador són:
    
    1. GHz
    2. Cores
    3. Marca
    4. USB
    5. Refrigeració
    6. Temperatura
    7. Socket
    8. Compatibilidad / BUS

Un procesador esta fet amb: Silici (multiples capas)

¿Cuantas fabricas hay en el undo de procesadores?
¿Y donde y porque?

Los cores a su maxima velocidad necesita refrigerar, tanto on un radiador como con una refigeración liquida.

### Overclocking
Sirve para forzar la velocidad y superar la velocidad dicha por el fabricante.

### Cores (set de instrucciones)
**Cisc** = Instrucciones complejas pero más lentas
**Risc** = Instrucciones sencillas pero más rapidas.
Sirven para repartir el trabajo

```
(Arquitecturas)
i386 y x64 = Cisc
Risc = ARM

```
### Threads (hilos) 
Hilo de instrucciones; Como el que dice la cola de trabajo (Dependeiente de tienda)

### Instalar Software
    Gestor de paquetes
        Cada distribución tienen un gestor de paquetes:
        Ubuntu: apt
            Tipo de paquete .deb
        Fedora: dnf
            Tipo de paquete .rpm
    
    SHORTCUTS per la terminal
        moure's per la sortida del terminal
            ```
            mayus + Re Pag
            mayus + Av Pag

            ```
    NETEJAR terminal = ctrl + l o clear
    SORTIR de una execució = ctrl + c
    SORTIR del termianl = ctrl + o
```
dnf search ( - )
```
Busca si existe ( - ) este nombre.
Descarrega la info. actualizada dels repositoris.
*Repositoris* = Servidor amb una col·lecciṕ de software.
    Repositoris oficials.
    Repositoris (altres)

--help = serveix per informar de les opcions principals amb una petita descripció

man = llistar el manual amb la informació més extensa. Obre un lector de text en pantalla (less)

```
dnf -y install htop lm_sensors
```

top = monitor de procesos
